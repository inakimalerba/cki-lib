"""Multiprocessing Pool for non-daemonic processes."""

import multiprocessing
from multiprocessing.pool import Pool


class NonDaemonicProcess(multiprocessing.Process):
    """Non-daemonic process for purposes of being used in a pool."""

    @property
    def daemon(self):
        """Property that tells whether process is daemonic."""
        return False

    @daemon.setter
    def daemon(self, _):
        """Don't allow to change the process type to daemonic."""


class NonDaemonicContext(type(multiprocessing.get_context())):
    # pylint: disable=R0903
    """Non-daemonic context for multiprocessing.pool.Pool."""

    Process = NonDaemonicProcess


class NonDaemonicPool(Pool):
    # pylint: disable=W0223
    """A pool of non-daemonic processes."""

    def __init__(self, *args, **kwargs):
        """Create an object."""
        kwargs['context'] = NonDaemonicContext()
        super(NonDaemonicPool, self).__init__(*args, **kwargs)
