"""Misc utility functions."""

import collections
import fcntl
import glob
import logging
import os
import pathlib
import mailbox
import subprocess
import sys
import tempfile
from configparser import ConfigParser
from contextlib import contextmanager
from email.header import decode_header

from cki_lib.retrying import retrying_on_exception

CKI_LOGS_FILEDIR = '/tmp/cki-lib.log/'


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""


def normalize_subject(subject):
    """Decode subject of an email, regardless of encoding."""
    result = ''
    for text, encoding in decode_header(subject):
        if not encoding:
            if isinstance(text, bytes):
                result += text.decode('utf-8')
            else:
                result += text
        else:
            result += text.decode(encoding)

    return result


def get_env_var_or_raise(env_key):
    """Retrieve the value of an environment variable or raise exception.

    Args:
        env_key:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.

    """
    env_var = os.getenv(env_key)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {env_key} is not set!')
    return env_var


@contextmanager
def tempfile_from_string(data):
    """Create a tempfile that is deleted on contextmanager exit.

    Arguments:
        data: str, a string that the tempfile will contain

    """
    temp = tempfile.NamedTemporaryFile(delete=False)
    temp.write(data)
    temp.close()
    try:
        yield temp.name
    finally:
        os.unlink(temp.name)


@contextmanager
def enter_dir(directory):
    """Change directory using os.chdir(directory), return on context exit.

    Arguments:
        directory: str, a directory to enter

    """
    current = os.getcwd()
    try:
        os.chdir(directory)
        yield
    finally:
        os.chdir(current)


def truncate_logs():
    """Truncate all .log files in the CKI_LOGS_FILEDIR directory."""
    files = glob.glob(f'{CKI_LOGS_FILEDIR}/*.log')
    # empty all logs
    safe_popen(['truncate', '-s', '0', ] + files)


def print_logfile(filename, strict=False):
    """Read filename in CKI_LOGS_FILEDIR and print it to stdout."""
    path = os.path.join(CKI_LOGS_FILEDIR, filename)
    if os.path.isfile(path):
        print(open(path, 'r').read())
    elif strict:
        raise RuntimeError(f'{path} is not a file!')


def logger_add_fhandler(logger, dst_file, path=None):
    """Add filehandler to existing logger."""
    dirname = CKI_LOGS_FILEDIR if path is None else path
    os.makedirs(dirname, exist_ok=True)
    dst_file = os.path.join(dirname, dst_file)

    fhandler = logging.FileHandler(dst_file)
    fhandler.setLevel(logging.DEBUG)
    logger.addHandler(fhandler)


def init_logger(logger_name, dst_file=None, stream_level=logging.WARNING,
                stream=None):
    """Init object for logging with a simple formatting string.

    Output goes to dst_file in the same directory as this script and
    logger_name is the name of the logger.

    Attributes:
        logger_name: str, something to identify distinct loggers
        dst_file: str, default is 'info.log', a filename to use for output
        stream_level: console output loglevel
        stream: None, sys.stderr or sys.stdout

    """
    logging.basicConfig(format="%(created)10.6f:%(levelname)s:%(message)s")
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    if dst_file is not None:
        logger_add_fhandler(logger, dst_file)

    shandler = logging.StreamHandler(stream if stream is not None else
                                     sys.stdout)
    shandler.setLevel(stream_level)
    logger.addHandler(shandler)

    return logger


def safe_popen(*args, stdin_data=None, **kwargs):
    """Open a process with specified arguments, keyword arguments, stdin data.

    This function blocks until process finishes. Uses utf-8 dst_file decode
    stdout/stderr, if there's any output on them.

    Arguments:
        args:       arguments dst_file pass dst_file Popen
        stdin_data: None or str, use None when you don't want dst_file pass
                    string data dst_file stdin
        kwargs:     keyword arguments dst_file pass dst_file Popen
    Returns:
        tuple (stdout, stderr, returncode) where
            stdout is a string
            stderr is a string
            returncode is an integer

    """
    subproc = subprocess.Popen(*args, **kwargs)

    stdout, stderr = subproc.communicate(stdin_data)
    stdout = stdout.decode('utf-8') if stdout else ''
    stderr = stderr.decode('utf-8') if stderr else ''

    return stdout, stderr, subproc.returncode


def read_stream(stream):
    """Read lines from stream like stdout."""
    fhandle = stream.fileno()
    flags = fcntl.fcntl(fhandle, fcntl.F_GETFL)
    fcntl.fcntl(fhandle, fcntl.F_SETFL, flags | os.O_NONBLOCK)
    try:
        data = stream.read()
        if data:
            return data.decode('utf-8')
    except OSError:
        pass
    return ""


@retrying_on_exception(RuntimeError)
def retry_safe_popen(err_exc_strings, *args, stdin_data=None, **kwargs):
    """ Call safe_popen with *args, stdin_data=None, **kwargs provided,
        If stderr stream is present and contains any string in err_exc_strings
        list, then the process call is done again with retry after 3 seconds
        (see retrying_on_exception decorator). Log commands retry and allow 3
        retries max. Also log if last command failed and we gave up. The
        program execution is not terminated / no exception is raised on last
        failure.

        Args:
            err_exc_strings: a list of strings; if any is present in stderr,
                             retry the command
            args:            arguments to pass to Popen
            stdin_data:      None or str, use None when you don't want to pass
                             string data to stdin
            kwargs:          keyword arguments to pass to Popen
        Returns:
            tuple (stdout, stderr, returncode) where
                stdout is a string
                stderr is a string
                returncode is an integer
    """
    stdout, stderr, returncode = safe_popen(*args, stdin_data=stdin_data,
                                            **kwargs)

    if err_exc_strings and stderr:
        # we clearly want to catch issues; let's debug what stderr was
        logging.warning(stderr.strip())

    for err_str in err_exc_strings:
        if stderr and err_str in stderr:
            logging.warning(f'caught "{err_str}" error string in stderr')
            raise RuntimeError

    return stdout, stderr, returncode


def parse_config_data(data, require_sections=True):
    """Parse config data (str or utf-8 bytes) into sectioned dict.

    The value under [section][key] is converted to int/float/str/list(str).

    Arguments:
        string: str or utf-8 bytes to read using ConfigParser
        require_sections: if False, wrap input in dummy section

    Returns:
        None or dict with all the values

    """
    if not data:
        return None

    parser = ConfigParser()
    # accept string or utf-8 bytes
    string = data if isinstance(data, str) else data.decode('utf-8')
    string = string if require_sections else ('[dummy]\n' + string)
    parser.read_string(string)

    # make 1st level of keys that match section names
    results = {section: {} for section in parser.sections()}

    for section in parser.sections():
        # make 2nd level of keys that match keys under respective sections
        for key, _ in parser.items(section, raw=True):
            # convert inner value, but don't convert 1/0 to True/False
            for func in [parser.getint, parser.getfloat, parser.get]:
                try:
                    values = func(section, key, raw=True)
                except ValueError:
                    continue
                else:
                    results[section][key] = values
                    break

    return results if require_sections else results['dummy']


def partition(seq, key):
    """Partition a sequence by 'key' criteria/condition."""
    def_dict = collections.defaultdict(list)
    for value in seq:
        def_dict[key(value)].append(value)
    return def_dict


def create_mbox_at(output, filename):
    """ Create empty file for mbox in dir output, file filename.
        Return mailbox.mbox for that file, so it can be edited.
        Args:
            output   - str, the path to the directory
            filename - str, the name of the file to create
        Returns:
            The new mailbox.mbox
    """
    path = os.path.join(output, filename)
    # create empty file
    pathlib.Path(path).touch()
    # use it as an empty mbox
    return mailbox.mbox(path)
