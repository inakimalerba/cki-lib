"""Logging for cki."""

import logging
import threading

LOCK = threading.Lock()
CKI_HANDLER = False
FORMAT = '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s'


def get_logger(name):
    """return cki logger or descendant"""

    # Make sure adding handler is thread safe since get_logger might not be
    # called from main
    # https://docs.python.org/3/library/logging.html#logging.basicConfig
    with LOCK:
        global CKI_HANDLER  # pylint: disable=global-statement
        cki_handler = CKI_HANDLER
        CKI_HANDLER = True

    if not cki_handler:
        cki_logger = logging.getLogger('cki')
        console = logging.StreamHandler()
        cki_logger.addHandler(console)
        formatter = logging.Formatter(FORMAT)
        console.setFormatter(formatter)

    if not (name == 'cki' or name.startswith('cki.')):
        name = 'cki.' + name

    return logging.getLogger(name)
