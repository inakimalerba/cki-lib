"""Misc api interaction tests."""
import io
import os
import subprocess
import unittest
from unittest import mock

from cki_lib import misc


class TestMisc(unittest.TestCase):
    """Test misc class."""

    def test_normalize_subject(self):
        """Test that decoding subject works."""
        subject1 = '=?utf-8?b?4p2M?= FAIL:'
        norm1 = misc.normalize_subject(subject1)
        self.assertEqual('❌ FAIL:', norm1)

        subject2 = '=?utf-8?B?4p2MIEZBSUw6IFJl?= =?utf-8?Q?=3A?='
        norm2 = misc.normalize_subject(subject2)
        self.assertEqual('❌ FAIL: Re:', norm2)

        subject3 = 'Waiting for review: ❌ SKIPPED: Re: [RHELX.Y] whatever:'
        norm3 = misc.normalize_subject(subject3)
        self.assertEqual(subject3, norm3)

    def test_get_env_var(self):
        """Ensure get_env_var_or_raise works."""
        with self.assertRaises(misc.EnvVarNotSetError):
            misc.get_env_var_or_raise('HOLY BULL IS REAL')

        key = None
        # select random first key from env
        for k in os.environ:
            key = k
            break

        # the function must return the same value as os.environ
        self.assertEqual(os.environ[key], misc.get_env_var_or_raise(key))

    def test_tempfile_from_string(self):
        """Ensure tempfile_from_string works."""
        test_data = b'a horse, or a cabbage'
        with misc.tempfile_from_string(test_data) as fname:
            self.assertTrue(os.path.isfile(fname))

            with open(fname, 'rb') as fobj:
                self.assertEqual(fobj.read(), test_data)

    def test_enter_dir(self):
        """Ensure enter_dir works."""
        current = os.getcwd()
        with misc.enter_dir('/tmp'):
            self.assertEqual(os.getcwd(), '/tmp')

        self.assertEqual(os.getcwd(), current)

    def test_truncate_logs(self):
        """Ensure truncate_logs works."""
        test_data = 'eat a hotdog'
        dst_file = 'unittesting.log'

        with open(os.path.join(misc.CKI_LOGS_FILEDIR, dst_file), 'w') as fobj:
            fobj.write(test_data)

        # empty all logfiles + check logfile we just changed is now empty
        misc.truncate_logs()
        with open(os.path.join(misc.CKI_LOGS_FILEDIR, dst_file), 'r') as fobj:
            self.assertEqual(fobj.read(), '')

    def test_safe_popen(self):
        """Ensure safe_popen works."""

        def fake_popen(*args, **kwargs):
            # pylint: disable=W0613
            fake_popen.communicate = lambda *a, **kw: (b'stdout data',
                                                       b'stderr data')
            fake_popen.returncode = 0
            fake_popen.called = True
            return fake_popen

        with mock.patch('subprocess.Popen', new_callable=fake_popen) as mpopen:
            stdout, stderr, retcode = misc.safe_popen(['echo', 'stdout data'],
                                                      stdout=subprocess.PIPE,
                                                      stderr=subprocess.PIPE)

            self.assertTrue(mpopen.called)
            self.assertEqual(stdout, 'stdout data')
            self.assertEqual(stderr, 'stderr data')
            self.assertEqual(retcode, 0)

    def test_retry_safe_popen(self):
        """Ensure retry_safe_popen works."""

        def fake_warn(*args, **kwargs):
            # pylint: disable=W0613
            assert ('HOLLY BULL' in arg or 'RETRY' in arg for arg in args)
            return fake_warn

        with mock.patch('logging.warning', fake_warn):
            with mock.patch('time.sleep', lambda x: x):
                with self.assertRaises(RuntimeError):
                    pycmd = "import sys; sys.stderr.write('HOLLY BULL')"
                    misc.retry_safe_popen(['HOLLY BULL'], ['python3', '-c',
                                                           pycmd],
                                          stderr=subprocess.PIPE)

        misc.retry_safe_popen([''], ['echo', 'duh'], stdout=subprocess.PIPE)

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_print_logfile(self, mock_stdout):
        """Ensure print_logfile works."""
        fname = 'yay.log'
        yay_logger = misc.init_logger(__name__, dst_file=fname)
        yay_logger.info('this works!')

        misc.print_logfile(fname)
        mock_stdout.seek(0)
        data = mock_stdout.read()
        self.assertIn('this works!', data)

        with self.assertRaises(RuntimeError):
            misc.print_logfile('HOLLY BULL', strict=True)

    @classmethod
    def test_print_logfile_permissive(cls):
        """Ensure non-strict print_logfile() doesn't raise anything."""
        misc.print_logfile('HOLLY BULL', strict=False)

    def test_parse_config_data(self):
        """Ensure parse_config_data works."""
        data = "key=value"

        result1 = misc.parse_config_data(data, False)
        self.assertEqual(result1['key'], 'value')

        result2 = misc.parse_config_data('[data]\n' + data, True)

        self.assertEqual(result2['data']['key'], 'value')

        result3 = misc.parse_config_data('[data]\na=b\n[data2]\nkey=value')
        self.assertEqual(result3['data2']['key'], 'value')

    def test_parse_config_data_empty(self):
        """Ensure parse_config_data returns None for empty."""
        result = misc.parse_config_data('')
        self.assertIsNone(result)

    def test_partition(self):
        """Ensure partition works."""
        job1 = {'name': 'abc', 'id': 1}
        job2 = {'name': 'abc', 'id': 2}
        job3 = {'name': 'gah', 'id': 3}

        jobs = [job1, job2, job3]

        result = misc.partition(jobs, lambda job: job['name'])
        self.assertEqual(result['abc'], [job1, job2])
        self.assertEqual(result['gah'], [job3])

    def test_read_stream(self):
        """Ensure read_stream works and does not block."""
        proc = subprocess.Popen(['echo', 'MAGIC'], stdout=subprocess.PIPE)

        result = ''
        while not result:
            result = misc.read_stream(proc.stdout)

        self.assertEqual('MAGIC', result.strip())
