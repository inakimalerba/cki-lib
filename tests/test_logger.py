"""logger tester."""
import io
import logging
import unittest
from unittest import mock

from cki_lib.logger import get_logger

# since get_logger will only ever add one handler,
# have to mock stderr classwide
MOCK_STDERR = io.StringIO()


@mock.patch('sys.stderr', new=MOCK_STDERR)
class TestLogger(unittest.TestCase):
    """Test logger class."""

    def test_propagation(self):
        """test inheritance and propagation"""
        MOCK_STDERR.truncate(0)

        cki_logger = get_logger('cki')
        mod_a_logger = get_logger('cki.mod_a')
        mod_b_logger = get_logger('cki.mod_b')

        # root will default to logging.WARNING
        cki_logger.setLevel(logging.WARNING)
        mod_a_logger.setLevel(logging.CRITICAL)
        mod_b_logger.setLevel(logging.DEBUG)

        # cki logs warning but not DEBUG
        cki_logger.warning('test_propagation')
        self.assertTrue(' - [WARNING] - cki - test_propagation'
                        in MOCK_STDERR.getvalue())
        MOCK_STDERR.truncate(0)
        cki_logger.debug('cki_logger')
        self.assertEqual(MOCK_STDERR.getvalue(), '')
        MOCK_STDERR.truncate(0)

        # mod_a doesn't log WARNING
        mod_a_logger.warning('mod_a_logger')
        self.assertEqual(MOCK_STDERR.getvalue(), '')
        MOCK_STDERR.truncate(0)

        # mod_b logs DEBUG
        mod_b_logger.debug('mod_b_logger')
        self.assertTrue(' - [DEBUG] - cki.mod_b - mod_b_logger'
                        in MOCK_STDERR.getvalue())

    def test_cki_prepended(self):
        """test cki prefix added to name if missing from passed name"""
        mod_a_logger = get_logger('mod_a')
        self.assertEqual(mod_a_logger.name, 'cki.mod_a')

    def test_handlers(self):
        """test only one handler is created"""
        MOCK_STDERR.truncate(0)
        logger1 = get_logger('logger1')
        get_logger('logger2')
        logger1.setLevel(logging.WARNING)
        logger1.warning('logger1')
        self.assertTrue(MOCK_STDERR.getvalue().count('\n') == 1)
