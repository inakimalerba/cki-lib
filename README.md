# cki-lib

Common place to put scripts used across CKI infrastructure.

## Maintainers

* [jracek](https://gitlab.com/jracek)
* [inakimalerba](https://gitlab.com/inakimalerba)

## Design principles

This repository hold code that is used within other CKI Project applications,
including:

* autowaive
* beaker-metrics
* bkr_respin
* data warehouse
* pipeline-definition
* pipeline-stats
* pipeline-trigger
* misc-tools
* reporter-ng
* skt
* umb messenger
* upt

## Overriding cki-lib in pipeline-definition

The used cki-lib version can be overriden using `cki_lib_targz_url` trigger
variable.

## Optional dependencies

To use the `teiid` module, use the `teiid` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-lib.git/#egg=cki_lib[teiid]
```

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

### Setup

If you want to get started with development, you can install the
dependencies by running:

```shell
pip install --user .[dev]
```

### Tests + linting

To run the tests (together with linting), execute the following command:

```shell
tox
```
